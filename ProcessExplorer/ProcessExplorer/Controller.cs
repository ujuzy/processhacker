﻿using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using Newtonsoft.Json;

namespace ProcessExplorer
{
    class Controller
    {
        public static List<MyProcess> GetFromJson()
        {
            List<MyProcess> result;

            using (StreamReader sr = new StreamReader(Globals.JsonPath))
            {
                result = JsonConvert.DeserializeObject<List<MyProcess>>(sr.ReadToEnd());
            }

            return result;
        }

        public static void StartConsole(string path, string arguments)
        {
            Process consoleApp = new Process();
            consoleApp.StartInfo.FileName = path;
            consoleApp.StartInfo.Arguments = arguments;
            consoleApp.Start();
        }

        public static Rights StringToRights(string right)
        {
            switch (right)
            {
                case "Full Control":
                    return Rights.FullControl;
                case "Append Data":
                    return Rights.AppendData;
                case "Read And Execute":
                    return Rights.ReadAndExecute;
                case "Read":
                    return Rights.Read;
                case "Write":
                    return Rights.Write;
            }

            return 0;
        }
    }
}