﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using ProcessPrivileges;

namespace Process_Explorer
{
    static class ProcessPrivileges
    {
        public static bool EnablePriv(Process process, string privilege)
        {
            if (privilege == "AssignPrimaryToken")
            {
                if (process.GetPrivilegeState(Privilege.AssignPrimaryToken) == PrivilegeState.Disabled)
                {
                    AdjustPrivilegeResult result = process.EnablePrivilege(Privilege.AssignPrimaryToken);

                    PrivilegeState takeOwnershipState = process.GetPrivilegeState(Privilege.AssignPrimaryToken);

                    return true;
                }
            }

            if (privilege == "Audit")
            {
                if (process.GetPrivilegeState(Privilege.Audit) == PrivilegeState.Disabled)
                {
                    AdjustPrivilegeResult result = process.EnablePrivilege(Privilege.Audit);

                    PrivilegeState takeOwnershipState = process.GetPrivilegeState(Privilege.Audit);

                    return true;
                }
            }

            if (privilege == "Backup")
            {
                if (process.GetPrivilegeState(Privilege.Backup) == PrivilegeState.Disabled)
                {
                    AdjustPrivilegeResult result = process.EnablePrivilege(Privilege.Backup);

                    PrivilegeState takeOwnershipState = process.GetPrivilegeState(Privilege.Backup);

                    return true;
                }
            }

            if (privilege == "ChangeNotify")
            {
                if (process.GetPrivilegeState(Privilege.ChangeNotify) == PrivilegeState.Disabled)
                {
                    AdjustPrivilegeResult result = process.EnablePrivilege(Privilege.ChangeNotify);

                    PrivilegeState takeOwnershipState = process.GetPrivilegeState(Privilege.ChangeNotify);

                    return true;
                }
            }

            if (privilege == "CreateGlobal")
            {
                if (process.GetPrivilegeState(Privilege.CreateGlobal) == PrivilegeState.Disabled)
                {
                    AdjustPrivilegeResult result = process.EnablePrivilege(Privilege.CreateGlobal);

                    PrivilegeState takeOwnershipState = process.GetPrivilegeState(Privilege.CreateGlobal);

                    return true;
                }
            }

            if (privilege == "CreatePageFile")
            {
                if (process.GetPrivilegeState(Privilege.CreatePageFile) == PrivilegeState.Disabled)
                {
                    AdjustPrivilegeResult result = process.EnablePrivilege(Privilege.CreatePageFile);

                    PrivilegeState takeOwnershipState = process.GetPrivilegeState(Privilege.CreatePageFile);

                    return true;
                }
            }

            if (privilege == "CreatePermanent")
            {
                if (process.GetPrivilegeState(Privilege.CreatePermanent) == PrivilegeState.Disabled)
                {
                    AdjustPrivilegeResult result = process.EnablePrivilege(Privilege.CreatePermanent);

                    PrivilegeState takeOwnershipState = process.GetPrivilegeState(Privilege.CreatePermanent);

                    return true;
                }
            }

            if (privilege == "CreateSymbolicLink")
            {
                if (process.GetPrivilegeState(Privilege.CreateSymbolicLink) == PrivilegeState.Disabled)
                {
                    AdjustPrivilegeResult result = process.EnablePrivilege(Privilege.CreateSymbolicLink);

                    PrivilegeState takeOwnershipState = process.GetPrivilegeState(Privilege.CreateSymbolicLink);

                    return true;
                }
            }

            if (privilege == "CreateToken")
            {
                if (process.GetPrivilegeState(Privilege.CreateToken) == PrivilegeState.Disabled)
                {
                    AdjustPrivilegeResult result = process.EnablePrivilege(Privilege.CreateToken);

                    PrivilegeState takeOwnershipState = process.GetPrivilegeState(Privilege.CreateToken);

                    return true;
                }
            }

            if (privilege == "Debug")
            {
                if (process.GetPrivilegeState(Privilege.Debug) == PrivilegeState.Disabled)
                {
                    AdjustPrivilegeResult result = process.EnablePrivilege(Privilege.Debug);

                    PrivilegeState takeOwnershipState = process.GetPrivilegeState(Privilege.Debug);

                    return true;
                }
            }

            if (privilege == "EnableDelegation")
            {
                if (process.GetPrivilegeState(Privilege.EnableDelegation) == PrivilegeState.Disabled)
                {
                    AdjustPrivilegeResult result = process.EnablePrivilege(Privilege.EnableDelegation);

                    PrivilegeState takeOwnershipState = process.GetPrivilegeState(Privilege.EnableDelegation);

                    return true;
                }
            }

            if (privilege == "Impersonate")
            {
                if (process.GetPrivilegeState(Privilege.Impersonate) == PrivilegeState.Disabled)
                {
                    AdjustPrivilegeResult result = process.EnablePrivilege(Privilege.Impersonate);

                    PrivilegeState takeOwnershipState = process.GetPrivilegeState(Privilege.Impersonate);

                    return true;
                }
            }

            if (privilege == "IncreaseBasePriority")
            {
                if (process.GetPrivilegeState(Privilege.IncreaseBasePriority) == PrivilegeState.Disabled)
                {
                    AdjustPrivilegeResult result = process.EnablePrivilege(Privilege.IncreaseBasePriority);

                    PrivilegeState takeOwnershipState = process.GetPrivilegeState(Privilege.IncreaseBasePriority);

                    return true;
                }
            }

            if (privilege == "IncreaseQuota")
            {
                if (process.GetPrivilegeState(Privilege.IncreaseQuota) == PrivilegeState.Disabled)
                {
                    AdjustPrivilegeResult result = process.EnablePrivilege(Privilege.IncreaseQuota);

                    PrivilegeState takeOwnershipState = process.GetPrivilegeState(Privilege.IncreaseQuota);

                    return true;
                }
            }

            if (privilege == "IncreaseWorkingSet")
            {
                if (process.GetPrivilegeState(Privilege.IncreaseWorkingSet) == PrivilegeState.Disabled)
                {
                    AdjustPrivilegeResult result = process.EnablePrivilege(Privilege.IncreaseWorkingSet);

                    PrivilegeState takeOwnershipState = process.GetPrivilegeState(Privilege.IncreaseWorkingSet);

                    return true;
                }
            }

            if (privilege == "LoadDriver")
            {
                if (process.GetPrivilegeState(Privilege.LoadDriver) == PrivilegeState.Disabled)
                {
                    AdjustPrivilegeResult result = process.EnablePrivilege(Privilege.LoadDriver);

                    PrivilegeState takeOwnershipState = process.GetPrivilegeState(Privilege.LoadDriver);

                    return true;
                }
            }

            if (privilege == "LockMemory")
            {
                if (process.GetPrivilegeState(Privilege.LockMemory) == PrivilegeState.Disabled)
                {
                    AdjustPrivilegeResult result = process.EnablePrivilege(Privilege.LockMemory);

                    PrivilegeState takeOwnershipState = process.GetPrivilegeState(Privilege.LockMemory);

                    return true;
                }
            }

            if (privilege == "MachineAccount")
            {
                if (process.GetPrivilegeState(Privilege.MachineAccount) == PrivilegeState.Disabled)
                {
                    AdjustPrivilegeResult result = process.EnablePrivilege(Privilege.MachineAccount);

                    PrivilegeState takeOwnershipState = process.GetPrivilegeState(Privilege.MachineAccount);

                    return true;
                }
            }

            if (privilege == "ManageVolume")
            {
                if (process.GetPrivilegeState(Privilege.ManageVolume) == PrivilegeState.Disabled)
                {
                    AdjustPrivilegeResult result = process.EnablePrivilege(Privilege.ManageVolume);

                    PrivilegeState takeOwnershipState = process.GetPrivilegeState(Privilege.ManageVolume);

                    return true;
                }
            }

            if (privilege == "ProfileSingleProcess")
            {
                if (process.GetPrivilegeState(Privilege.ProfileSingleProcess) == PrivilegeState.Disabled)
                {
                    AdjustPrivilegeResult result = process.EnablePrivilege(Privilege.ProfileSingleProcess);

                    PrivilegeState takeOwnershipState = process.GetPrivilegeState(Privilege.ProfileSingleProcess);

                    return true;
                }
            }

            if (privilege == "Relabel")
            {
                if (process.GetPrivilegeState(Privilege.Relabel) == PrivilegeState.Disabled)
                {
                    AdjustPrivilegeResult result = process.EnablePrivilege(Privilege.Relabel);

                    PrivilegeState takeOwnershipState = process.GetPrivilegeState(Privilege.Relabel);

                    return true;
                }
            }

            if (privilege == "RemoteShutdown")
            {
                if (process.GetPrivilegeState(Privilege.RemoteShutdown) == PrivilegeState.Disabled)
                {
                    AdjustPrivilegeResult result = process.EnablePrivilege(Privilege.RemoteShutdown);

                    PrivilegeState takeOwnershipState = process.GetPrivilegeState(Privilege.RemoteShutdown);

                    return true;
                }
            }

            if (privilege == "Restore")
            {
                if (process.GetPrivilegeState(Privilege.Restore) == PrivilegeState.Disabled)
                {
                    AdjustPrivilegeResult result = process.EnablePrivilege(Privilege.Restore);

                    PrivilegeState takeOwnershipState = process.GetPrivilegeState(Privilege.Restore);

                    return true;
                }
            }

            if (privilege == "Security")
            {
                if (process.GetPrivilegeState(Privilege.Security) == PrivilegeState.Disabled)
                {
                    AdjustPrivilegeResult result = process.EnablePrivilege(Privilege.Security);

                    PrivilegeState takeOwnershipState = process.GetPrivilegeState(Privilege.Security);

                    return true;
                }
            }

            if (privilege == "Shutdown")
            {
                if (process.GetPrivilegeState(Privilege.Shutdown) == PrivilegeState.Disabled)
                {
                    AdjustPrivilegeResult result = process.EnablePrivilege(Privilege.Shutdown);

                    PrivilegeState takeOwnershipState = process.GetPrivilegeState(Privilege.Shutdown);

                    return true;
                }
            }

            if (privilege == "SyncAgent")
            {
                if (process.GetPrivilegeState(Privilege.SyncAgent) == PrivilegeState.Disabled)
                {
                    AdjustPrivilegeResult result = process.EnablePrivilege(Privilege.SyncAgent);

                    PrivilegeState takeOwnershipState = process.GetPrivilegeState(Privilege.SyncAgent);

                    return true;
                }
            }

            if (privilege == "SystemEnvironment")
            {
                if (process.GetPrivilegeState(Privilege.SystemEnvironment) == PrivilegeState.Disabled)
                {
                    AdjustPrivilegeResult result = process.EnablePrivilege(Privilege.SystemEnvironment);

                    PrivilegeState takeOwnershipState = process.GetPrivilegeState(Privilege.SystemEnvironment);

                    return true;
                }
            }

            if (privilege == "SystemProfile")
            {
                if (process.GetPrivilegeState(Privilege.SystemProfile) == PrivilegeState.Disabled)
                {
                    AdjustPrivilegeResult result = process.EnablePrivilege(Privilege.SystemProfile);

                    PrivilegeState takeOwnershipState = process.GetPrivilegeState(Privilege.SystemProfile);

                    return true;
                }
            }

            if (privilege == "SystemTime")
            {
                if (process.GetPrivilegeState(Privilege.SystemTime) == PrivilegeState.Disabled)
                {
                    AdjustPrivilegeResult result = process.EnablePrivilege(Privilege.SystemTime);

                    PrivilegeState takeOwnershipState = process.GetPrivilegeState(Privilege.SystemTime);

                    return true;
                }
            }

            if (privilege == "TakeOwnership")
            {
                if (process.GetPrivilegeState(Privilege.TakeOwnership) == PrivilegeState.Disabled)
                {
                    AdjustPrivilegeResult result = process.EnablePrivilege(Privilege.TakeOwnership);

                    PrivilegeState takeOwnershipState = process.GetPrivilegeState(Privilege.TakeOwnership);

                    return true;
                }
            }

            if (privilege == "TrustedComputerBase")
            {
                if (process.GetPrivilegeState(Privilege.TrustedComputerBase) == PrivilegeState.Disabled)
                {
                    AdjustPrivilegeResult result = process.EnablePrivilege(Privilege.TrustedComputerBase);

                    PrivilegeState takeOwnershipState = process.GetPrivilegeState(Privilege.TrustedComputerBase);

                    return true;
                }
            }

            if (privilege == "TimeZone")
            {
                if (process.GetPrivilegeState(Privilege.TimeZone) == PrivilegeState.Disabled)
                {
                    AdjustPrivilegeResult result = process.EnablePrivilege(Privilege.TimeZone);

                    PrivilegeState takeOwnershipState = process.GetPrivilegeState(Privilege.TimeZone);

                    return true;
                }
            }

            if (privilege == "TrustedCredentialManagerAccess")
            {
                if (process.GetPrivilegeState(Privilege.TrustedCredentialManagerAccess) == PrivilegeState.Disabled)
                {
                    AdjustPrivilegeResult result = process.EnablePrivilege(Privilege.TrustedCredentialManagerAccess);

                    PrivilegeState takeOwnershipState = process.GetPrivilegeState(Privilege.TrustedCredentialManagerAccess);

                    return true;
                }
            }

            if (privilege == "Undock")
            {
                if (process.GetPrivilegeState(Privilege.Undock) == PrivilegeState.Disabled)
                {
                    AdjustPrivilegeResult result = process.EnablePrivilege(Privilege.Undock);

                    PrivilegeState takeOwnershipState = process.GetPrivilegeState(Privilege.Undock);

                    return true;
                }
            }

            if (privilege == "UnsolicitedInput")
            {
                if (process.GetPrivilegeState(Privilege.UnsolicitedInput) == PrivilegeState.Disabled)
                {
                    AdjustPrivilegeResult result = process.EnablePrivilege(Privilege.UnsolicitedInput);

                    PrivilegeState takeOwnershipState = process.GetPrivilegeState(Privilege.UnsolicitedInput);

                    return true;
                }
            }

            return false;
        }

        public static bool DisablePriv(Process process, string privilege)
        {
            if (privilege == "AssignPrimaryToken")
            {
                if (process.GetPrivilegeState(Privilege.AssignPrimaryToken) == PrivilegeState.Enabled)
                {
                    AdjustPrivilegeResult result = process.DisablePrivilege(Privilege.AssignPrimaryToken);

                    PrivilegeState takeOwnershipState = process.GetPrivilegeState(Privilege.AssignPrimaryToken);

                    return true;
                }
            }

            if (privilege == "Audit")
            {
                if (process.GetPrivilegeState(Privilege.Audit) == PrivilegeState.Enabled)
                {
                    AdjustPrivilegeResult result = process.DisablePrivilege(Privilege.Audit);

                    PrivilegeState takeOwnershipState = process.GetPrivilegeState(Privilege.Audit);

                    return true;
                }
            }

            if (privilege == "Backup")
            {
                if (process.GetPrivilegeState(Privilege.Backup) == PrivilegeState.Enabled)
                {
                    AdjustPrivilegeResult result = process.DisablePrivilege(Privilege.Backup);

                    PrivilegeState takeOwnershipState = process.GetPrivilegeState(Privilege.Backup);

                    return true;
                }
            }

            if (privilege == "ChangeNotify")
            {
                if (process.GetPrivilegeState(Privilege.ChangeNotify) == PrivilegeState.Enabled)
                {
                    AdjustPrivilegeResult result = process.DisablePrivilege(Privilege.ChangeNotify);

                    PrivilegeState takeOwnershipState = process.GetPrivilegeState(Privilege.ChangeNotify);

                    return true;
                }
            }

            if (privilege == "CreateGlobal")
            {
                if (process.GetPrivilegeState(Privilege.CreateGlobal) == PrivilegeState.Enabled)
                {
                    AdjustPrivilegeResult result = process.DisablePrivilege(Privilege.CreateGlobal);

                    PrivilegeState takeOwnershipState = process.GetPrivilegeState(Privilege.CreateGlobal);

                    return true;
                }
            }

            if (privilege == "CreatePageFile")
            {
                if (process.GetPrivilegeState(Privilege.CreatePageFile) == PrivilegeState.Enabled)
                {
                    AdjustPrivilegeResult result = process.DisablePrivilege(Privilege.CreatePageFile);

                    PrivilegeState takeOwnershipState = process.GetPrivilegeState(Privilege.CreatePageFile);

                    return true;
                }
            }

            if (privilege == "CreatePermanent")
            {
                if (process.GetPrivilegeState(Privilege.CreatePermanent) == PrivilegeState.Enabled)
                {
                    AdjustPrivilegeResult result = process.DisablePrivilege(Privilege.CreatePermanent);

                    PrivilegeState takeOwnershipState = process.GetPrivilegeState(Privilege.CreatePermanent);

                    return true;
                }
            }

            if (privilege == "CreateSymbolicLink")
            {
                if (process.GetPrivilegeState(Privilege.CreateSymbolicLink) == PrivilegeState.Enabled)
                {
                    AdjustPrivilegeResult result = process.DisablePrivilege(Privilege.CreateSymbolicLink);

                    PrivilegeState takeOwnershipState = process.GetPrivilegeState(Privilege.CreateSymbolicLink);

                    return true;
                }
            }

            if (privilege == "CreateToken")
            {
                if (process.GetPrivilegeState(Privilege.CreateToken) == PrivilegeState.Enabled)
                {
                    AdjustPrivilegeResult result = process.DisablePrivilege(Privilege.CreateToken);

                    PrivilegeState takeOwnershipState = process.GetPrivilegeState(Privilege.CreateToken);

                    return true;
                }
            }

            if (privilege == "Debug")
            {
                if (process.GetPrivilegeState(Privilege.Debug) == PrivilegeState.Enabled)
                {
                    AdjustPrivilegeResult result = process.DisablePrivilege(Privilege.Debug);

                    PrivilegeState takeOwnershipState = process.GetPrivilegeState(Privilege.Debug);

                    return true;
                }
            }

            if (privilege == "EnableDelegation")
            {
                if (process.GetPrivilegeState(Privilege.EnableDelegation) == PrivilegeState.Enabled)
                {
                    AdjustPrivilegeResult result = process.DisablePrivilege(Privilege.EnableDelegation);

                    PrivilegeState takeOwnershipState = process.GetPrivilegeState(Privilege.EnableDelegation);

                    return true;
                }
            }

            if (privilege == "Impersonate")
            {
                if (process.GetPrivilegeState(Privilege.Impersonate) == PrivilegeState.Enabled)
                {
                    AdjustPrivilegeResult result = process.DisablePrivilege(Privilege.Impersonate);

                    PrivilegeState takeOwnershipState = process.GetPrivilegeState(Privilege.Impersonate);

                    return true;
                }
            }

            if (privilege == "IncreaseBasePriority")
            {
                if (process.GetPrivilegeState(Privilege.IncreaseBasePriority) == PrivilegeState.Enabled)
                {
                    AdjustPrivilegeResult result = process.DisablePrivilege(Privilege.IncreaseBasePriority);

                    PrivilegeState takeOwnershipState = process.GetPrivilegeState(Privilege.IncreaseBasePriority);

                    return true;
                }
            }

            if (privilege == "IncreaseQuota")
            {
                if (process.GetPrivilegeState(Privilege.IncreaseQuota) == PrivilegeState.Enabled)
                {
                    AdjustPrivilegeResult result = process.DisablePrivilege(Privilege.IncreaseQuota);

                    PrivilegeState takeOwnershipState = process.GetPrivilegeState(Privilege.IncreaseQuota);

                    return true;
                }
            }

            if (privilege == "IncreaseWorkingSet")
            {
                if (process.GetPrivilegeState(Privilege.IncreaseWorkingSet) == PrivilegeState.Enabled)
                {
                    AdjustPrivilegeResult result = process.DisablePrivilege(Privilege.IncreaseWorkingSet);

                    PrivilegeState takeOwnershipState = process.GetPrivilegeState(Privilege.IncreaseWorkingSet);

                    return true;
                }
            }

            if (privilege == "LoadDriver")
            {
                if (process.GetPrivilegeState(Privilege.LoadDriver) == PrivilegeState.Enabled)
                {
                    AdjustPrivilegeResult result = process.DisablePrivilege(Privilege.LoadDriver);

                    PrivilegeState takeOwnershipState = process.GetPrivilegeState(Privilege.LoadDriver);

                    return true;
                }
            }

            if (privilege == "LockMemory")
            {
                if (process.GetPrivilegeState(Privilege.LockMemory) == PrivilegeState.Enabled)
                {
                    AdjustPrivilegeResult result = process.DisablePrivilege(Privilege.LockMemory);

                    PrivilegeState takeOwnershipState = process.GetPrivilegeState(Privilege.LockMemory);

                    return true;
                }
            }

            if (privilege == "MachineAccount")
            {
                if (process.GetPrivilegeState(Privilege.MachineAccount) == PrivilegeState.Enabled)
                {
                    AdjustPrivilegeResult result = process.DisablePrivilege(Privilege.MachineAccount);

                    PrivilegeState takeOwnershipState = process.GetPrivilegeState(Privilege.MachineAccount);

                    return true;
                }
            }

            if (privilege == "ManageVolume")
            {
                if (process.GetPrivilegeState(Privilege.ManageVolume) == PrivilegeState.Enabled)
                {
                    AdjustPrivilegeResult result = process.DisablePrivilege(Privilege.ManageVolume);

                    PrivilegeState takeOwnershipState = process.GetPrivilegeState(Privilege.ManageVolume);

                    return true;
                }
            }

            if (privilege == "ProfileSingleProcess")
            {
                if (process.GetPrivilegeState(Privilege.ProfileSingleProcess) == PrivilegeState.Enabled)
                {
                    AdjustPrivilegeResult result = process.DisablePrivilege(Privilege.ProfileSingleProcess);

                    PrivilegeState takeOwnershipState = process.GetPrivilegeState(Privilege.ProfileSingleProcess);

                    return true;
                }
            }

            if (privilege == "Relabel")
            {
                if (process.GetPrivilegeState(Privilege.Relabel) == PrivilegeState.Enabled)
                {
                    AdjustPrivilegeResult result = process.DisablePrivilege(Privilege.Relabel);

                    PrivilegeState takeOwnershipState = process.GetPrivilegeState(Privilege.Relabel);

                    return true;
                }
            }

            if (privilege == "RemoteShutdown")
            {
                if (process.GetPrivilegeState(Privilege.RemoteShutdown) == PrivilegeState.Enabled)
                {
                    AdjustPrivilegeResult result = process.DisablePrivilege(Privilege.RemoteShutdown);

                    PrivilegeState takeOwnershipState = process.GetPrivilegeState(Privilege.RemoteShutdown);

                    return true;
                }
            }

            if (privilege == "Restore")
            {
                if (process.GetPrivilegeState(Privilege.Restore) == PrivilegeState.Enabled)
                {
                    AdjustPrivilegeResult result = process.DisablePrivilege(Privilege.Restore);

                    PrivilegeState takeOwnershipState = process.GetPrivilegeState(Privilege.Restore);

                    return true;
                }
            }

            if (privilege == "Security")
            {
                if (process.GetPrivilegeState(Privilege.Security) == PrivilegeState.Enabled)
                {
                    AdjustPrivilegeResult result = process.DisablePrivilege(Privilege.Security);

                    PrivilegeState takeOwnershipState = process.GetPrivilegeState(Privilege.Security);

                    return true;
                }
            }

            if (privilege == "Shutdown")
            {
                if (process.GetPrivilegeState(Privilege.Shutdown) == PrivilegeState.Enabled)
                {
                    AdjustPrivilegeResult result = process.DisablePrivilege(Privilege.Shutdown);

                    PrivilegeState takeOwnershipState = process.GetPrivilegeState(Privilege.Shutdown);

                    return true;
                }
            }

            if (privilege == "SyncAgent")
            {
                if (process.GetPrivilegeState(Privilege.SyncAgent) == PrivilegeState.Enabled)
                {
                    AdjustPrivilegeResult result = process.DisablePrivilege(Privilege.SyncAgent);

                    PrivilegeState takeOwnershipState = process.GetPrivilegeState(Privilege.SyncAgent);

                    return true;
                }
            }

            if (privilege == "SystemEnvironment")
            {
                if (process.GetPrivilegeState(Privilege.SystemEnvironment) == PrivilegeState.Enabled)
                {
                    AdjustPrivilegeResult result = process.DisablePrivilege(Privilege.SystemEnvironment);

                    PrivilegeState takeOwnershipState = process.GetPrivilegeState(Privilege.SystemEnvironment);

                    return true;
                }
            }

            if (privilege == "SystemProfile")
            {
                if (process.GetPrivilegeState(Privilege.SystemProfile) == PrivilegeState.Enabled)
                {
                    AdjustPrivilegeResult result = process.DisablePrivilege(Privilege.SystemProfile);

                    PrivilegeState takeOwnershipState = process.GetPrivilegeState(Privilege.SystemProfile);

                    return true;
                }
            }

            if (privilege == "SystemTime")
            {
                if (process.GetPrivilegeState(Privilege.SystemTime) == PrivilegeState.Enabled)
                {
                    AdjustPrivilegeResult result = process.DisablePrivilege(Privilege.SystemTime);

                    PrivilegeState takeOwnershipState = process.GetPrivilegeState(Privilege.SystemTime);

                    return true;
                }
            }

            if (privilege == "TakeOwnership")
            {
                if (process.GetPrivilegeState(Privilege.TakeOwnership) == PrivilegeState.Enabled)
                {
                    AdjustPrivilegeResult result = process.DisablePrivilege(Privilege.TakeOwnership);

                    PrivilegeState takeOwnershipState = process.GetPrivilegeState(Privilege.TakeOwnership);

                    return true;
                }
            }

            if (privilege == "TrustedComputerBase")
            {
                if (process.GetPrivilegeState(Privilege.TrustedComputerBase) == PrivilegeState.Enabled)
                {
                    AdjustPrivilegeResult result = process.DisablePrivilege(Privilege.TrustedComputerBase);

                    PrivilegeState takeOwnershipState = process.GetPrivilegeState(Privilege.TrustedComputerBase);

                    return true;
                }
            }

            if (privilege == "TimeZone")
            {
                if (process.GetPrivilegeState(Privilege.TimeZone) == PrivilegeState.Enabled)
                {
                    AdjustPrivilegeResult result = process.DisablePrivilege(Privilege.TimeZone);

                    PrivilegeState takeOwnershipState = process.GetPrivilegeState(Privilege.TimeZone);

                    return true;
                }
            }

            if (privilege == "TrustedCredentialManagerAccess")
            {
                if (process.GetPrivilegeState(Privilege.TrustedCredentialManagerAccess) == PrivilegeState.Enabled)
                {
                    AdjustPrivilegeResult result = process.DisablePrivilege(Privilege.TrustedCredentialManagerAccess);

                    PrivilegeState takeOwnershipState = process.GetPrivilegeState(Privilege.TrustedCredentialManagerAccess);

                    return true;
                }
            }

            if (privilege == "Undock")
            {
                if (process.GetPrivilegeState(Privilege.Undock) == PrivilegeState.Enabled)
                {
                    AdjustPrivilegeResult result = process.DisablePrivilege(Privilege.Undock);

                    PrivilegeState takeOwnershipState = process.GetPrivilegeState(Privilege.Undock);

                    return true;
                }
            }

            if (privilege == "UnsolicitedInput")
            {
                if (process.GetPrivilegeState(Privilege.UnsolicitedInput) == PrivilegeState.Enabled)
                {
                    AdjustPrivilegeResult result = process.DisablePrivilege(Privilege.UnsolicitedInput);

                    PrivilegeState takeOwnershipState = process.GetPrivilegeState(Privilege.UnsolicitedInput);

                    return true;
                }
            }

            return false;
        }

        public static Dictionary<string, string> ShowPrivileges(Process process)
        {
            try
            {
                Dictionary<string, string> priv = new Dictionary<string, string>();
                PrivilegeAndAttributesCollection privileges = process.GetPrivileges();
                int maxPrivilegeLength = privileges.Max(privilege => privilege.Privilege.ToString().Length);
                foreach (PrivilegeAndAttributes privilegeAndAttributes in privileges)
                {
                    // The privilege.
                    Privilege privilege = privilegeAndAttributes.Privilege;

                    // The privilege state.
                    PrivilegeState privilegeState = privilegeAndAttributes.PrivilegeState;

                    // Write out the privilege and its state.
                    priv.Add(privilege.ToString(), privilegeState.ToString());
                }
                return priv;
            }
            catch (System.ComponentModel.Win32Exception)
            {
                return new Dictionary<string, string>();
            }
            catch (System.InvalidOperationException)
            {
                return new Dictionary<string, string>();
            }
            catch (System.ArgumentException)
            {
                return new Dictionary<string, string>();
            }
        }
    }
}
