﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Documents;

namespace ProcessExplorer
{
    public partial class FileInfo : Window
    {
        private MyFile mFile;

        public FileInfo(string path)
        {
            InitializeComponent();

            mFile = new MyFile(path);

            filePath.Text = mFile.PathFile;
            ownerName.Text = mFile.GetOwner();
            integrityLevel.Text = mFile.IntegrityLevel.ToString();

            List<string> acl = mFile.GetAcl();
            List<Kek> kek = new List<Kek>();

            foreach (var e in acl)
            {
                kek.Add(new Kek(e));
            }

            aclList.ItemsSource = kek;
        }

        private void ButtonAdd_OnClick(object sender, RoutedEventArgs e)
        {
            AclWindow newWindow = new AclWindow(mFile);

            newWindow.Show();
        }

        private void MenuItemDeleteAcl_OnClick(object sender, RoutedEventArgs e)
        {
            var aclInfo = (aclList.SelectedItem as Kek).String;

            string[] separators = { ", " };
            string[] aclArguments = aclInfo.Split(separators, 4, StringSplitOptions.RemoveEmptyEntries);

            MyFile.DelAcl(aclArguments[0], MyFile.GetAclType(aclArguments[2]), aclArguments[1], mFile.PathFile);
        }

        private void ButtonOwnerChange_OnClick(object sender, RoutedEventArgs e)
        {
            //mFile.ChangeOwner(ownerName.Text, "");
            string ewq = "";

            try
            {
                ewq = MyFile.usernameToSid(ownerName.Text).ToString();
                Console.WriteLine(ewq);
                MyFile.setOwner(Encoding.Unicode.GetBytes(ewq), Encoding.Unicode.GetBytes(mFile.PathFile));
            }
            catch
            {
                Console.WriteLine(ewq);
            }
        }

        private void ButtonIntegrityChange_OnClick(object sender, RoutedEventArgs e)
        {
            if (integrityLevel.Text != "1" && integrityLevel.Text != "2" && integrityLevel.Text != "3" &&
                integrityLevel.Text != "4" && integrityLevel.Text != "5")
            {
                integrityLevel.Text = "Inappropriate value";
            } 
            else 
            {
                if (integrityLevel.Text == "5")
                {
                    integrityLevel.Text = "You cant set \"System\" integrity level";
                } 
                else
                {
                    bool check = MyFile.setIntegrityLevelFile(Int32.Parse(integrityLevel.Text), mFile.PathBytes);
                    Console.WriteLine(check);
                }

            }

        }
    }

    public class Kek
    { 
        public string String { get; set; }

        public Kek (string String)
        {
            this.String = String;
        }
    }
}