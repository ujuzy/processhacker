﻿using System;
using System.Text;
using System.IO;
using System.Collections.Generic;
using System.Security.AccessControl;
using System.Security.Principal;
using Newtonsoft.Json;
using System.Runtime.InteropServices;

namespace Process_Explorer
{
    enum Rights
    {
        FullControl,
        AppendData,
        ReadAndExecute,
        Read,
        Write
    }

    class MyFile
    {
        [DllImport("FileDll.dll")]
        [return: MarshalAs(UnmanagedType.I4)]
        internal static extern int integrityLevelFile(string path);

        [JsonProperty("PathFile;")]
        public string mPathFile;

        [JsonProperty("Owner")]
        public string Owner { get; }

        [JsonProperty("IntegrityLevel")]
        public int IntegrityLevel { get; }

        [JsonProperty("ACL")]
        public List<string> Acl { get; }

        public MyFile(string pathFile)
        {
            this.mPathFile = pathFile;
            Owner = GetOwner();
            IntegrityLevel = (integrityLevelFile(pathFile));
            Acl = GetAcl();
        }

        public string GetOwner()
        {
            var fs = File.GetAccessControl(mPathFile);
            var sid = fs.GetOwner(typeof(SecurityIdentifier));
            var ntAccount = sid.Translate(typeof(NTAccount));
            return ntAccount.ToString();
        }

        public bool ChangeOwner(string domain, string username)
        {
            var ntAccount = new NTAccount(domain, username);
            var fs = File.GetAccessControl(mPathFile);
            fs.SetOwner(ntAccount);
            File.SetAccessControl(mPathFile, fs);
            return true;
        }

        public List<string> GetAcl()
        {
            List<string> myList = new List<string>();
            using (FileStream myFile = new FileStream(
                    mPathFile, FileMode.Open, FileAccess.Read))
            {
                FileSecurity fileSec = myFile.GetAccessControl();
                Console.WriteLine("Список ACL: ");
                foreach (FileSystemAccessRule fileRule in
                    fileSec.GetAccessRules(true, true, typeof(NTAccount)))
                {
                    myList.Add((String.Format("{0} {1} доступ {2} для {3}", mPathFile,
                        fileRule.AccessControlType == AccessControlType.Allow ? "разрешен" : "запрещен",
                        fileRule.FileSystemRights, fileRule.IdentityReference)));
                }
            }
            return myList;
        }
        //permission - true is Allows
        public static bool SetAcl(string account, string permission, Rights rights, string path)
        {
            var permis = AccessControlType.Allow;
            if (permission == "deny")
            {
                permis = AccessControlType.Deny;
            }
            FileSystemAccessRule newRule = null;
            switch (rights)
            {
                case Rights.FullControl:
                    newRule = new FileSystemAccessRule(
                           new System.Security.Principal.NTAccount(account),
                           FileSystemRights.FullControl,
                           permis);
                    break;
                case Rights.AppendData:
                    newRule = new FileSystemAccessRule(
                            new System.Security.Principal.NTAccount(account),
                            FileSystemRights.AppendData,
                            permis);
                    break;
                case Rights.ReadAndExecute:
                    newRule = new FileSystemAccessRule(
                            new System.Security.Principal.NTAccount(account),
                            FileSystemRights.ReadAndExecute,
                            permis);
                    break;
                case Rights.Read:
                    newRule = new FileSystemAccessRule(
                            new System.Security.Principal.NTAccount(account),
                            FileSystemRights.Read,
                            permis);
                    break;
                case Rights.Write:
                    newRule = new FileSystemAccessRule(
                            new System.Security.Principal.NTAccount(account),
                            FileSystemRights.Write,
                            permis);
                    break;
                default:
                    return false;
            }
            using (FileStream myFile = new FileStream(
                    path, FileMode.Open, FileAccess.Read))
            {
                FileSecurity fileSec = myFile.GetAccessControl();
                fileSec.AddAccessRule(newRule);
                File.SetAccessControl(path, fileSec);
            }
            return true;
        }

        public static bool DelAcl(string domainAndName, FileSystemRights rights, string controlTypeString, string path)
        {
            AccessControlType controlType = AccessControlType.Allow;

            if (controlTypeString == "deny")
            {
                controlType = AccessControlType.Deny;
            }

            FileSecurity fSecurity = File.GetAccessControl(path);
            fSecurity.RemoveAccessRule(new FileSystemAccessRule(domainAndName,
                rights, controlType));
            File.SetAccessControl(path, fSecurity);
            return true;
        }
    }
}
