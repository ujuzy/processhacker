﻿using System;
using System.Security.Principal;
using System.Management;
using System.Diagnostics;
using Newtonsoft.Json;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;

namespace Process_Explorer
{
    public static class ProcessExtensions
    {
        public static bool IsWin64Emulator(this Process process)
        {
            if ((Environment.OSVersion.Version.Major > 5)
                || ((Environment.OSVersion.Version.Major == 5) && (Environment.OSVersion.Version.Minor >= 1)))
            {
                bool retVal;

                return NativeMethods.IsWow64Process(process.Handle, out retVal) && retVal;
            }

            return false; // not on 64-bit Windows Emulator
        }

        private static string FindIndexedProcessName(int pid)
        {
            var processName = Process.GetProcessById(pid).ProcessName;
            var processesByName = Process.GetProcessesByName(processName);
            string processIndexdName = null;

            for (var index = 0; index < processesByName.Length; index++)
            {
                processIndexdName = index == 0 ? processName : processName + "#" + index;
                var processId = new PerformanceCounter("Process", "ID Process", processIndexdName);
                if ((int)processId.NextValue() == pid)
                {
                    return processIndexdName;
                }
            }

            return processIndexdName;
        }

        private static Process FindPidFromIndexedProcessName(string indexedProcessName)
        {
            var parentId = new PerformanceCounter("Process", "Creating Process ID", indexedProcessName);
            return Process.GetProcessById((int)parentId.NextValue());
        }

        public static Process Parent(this Process process)
        {
            return FindPidFromIndexedProcessName(FindIndexedProcessName(process.Id));
        }
    }

    class MyProcess
    {
        [DllImport("MYDLL.dll")]
        [return: MarshalAs(UnmanagedType.I4)]
        internal static extern int integrityLevel(int pid);

        [DllImport("MYDLL.dll")]
        [return: MarshalAs(UnmanagedType.I4)]
        internal static extern int getppid(int pid);

        [JsonIgnore]
        public Process Process { get; }

        [JsonProperty("Id")]
        public int Id { get; }

        [JsonProperty("Descriprion")]
        public string Descriprion { get; }

        [JsonProperty("PathToExe")]
        public string PathToExe { get; }

        [JsonProperty("PName")]
        public string PName { get; }

        [JsonProperty("PId")]
        public int PId { get; }

        [JsonProperty("OwnerName")]
        public string OwnerName { get; }

        [JsonProperty("Sid")]
        public string Sid { get; }

        [JsonProperty("Bit")]
        public int Bit { get; }

        [JsonProperty("Environmen")]
        public string Environmen { get; }

        [JsonProperty("Dep")]
        public bool Dep { get; }

        [JsonProperty("Aslr")]
        public bool Aslr { get; }

        [JsonProperty("Dll")]
        public List<Module> Dll { get; }

        [JsonProperty("CountHand")]
        public int CountHand { get; }

        [JsonProperty("CountThread")]
        public int CountThread { get; }

        [JsonProperty("BasePrior")]
        public int BasePrior { get; }

        [JsonProperty("Privileges")]
        public Dictionary<string, string> Privileges { get; }

        [JsonProperty("Integrity")]
        public string Integrity { get; }

        public static string ConvertToString(int code)
        {
            switch (code)
            {
                case 1:
                    return "Untrusted";
                case 2:
                    return "Low";
                case 3:
                    return "Medium";
                case 4:
                    return "High";
                case 5:
                    return "System";
            }
            return null;
        }

        public static int ConvertToCode(string level)
        {
            switch (level)
            {
                case "Untrusted":
                    return 1;
                case "Low":
                    return 2;
                case "Medium":
                    return 3;
                case "High":
                    return 4;
                case "System":
                    return 5;
            }

            return 0;
        }

        private string HelpGetDescription()
        {
            try
            {
                var e = this.GetMainModuleFilepath();
                if (e == "No permission")
                {
                    return "No permission";
                }
                var fileVersionInfo = FileVersionInfo.GetVersionInfo(e.ToString());
                var processDescription = fileVersionInfo.FileDescription;
                return processDescription;
            }
            catch (System.IO.FileNotFoundException)
            {
                return "No file";
            }
            catch (System.ComponentModel.Win32Exception e)
            {
                return "No permission";
            }
        }

        public static SecurityIdentifier usernameToSid(string user)
        {
            try
            {
                if (user.StartsWith(@".\"))
                {
                    user = user.Replace(@".\", Environment.MachineName + @"\");
                }

                NTAccount account = new NTAccount(user);
                return (SecurityIdentifier)account.Translate(typeof(SecurityIdentifier));
            }
            catch (System.Security.Principal.IdentityNotMappedException)
            {
                return null;
            }
        }

        public MyProcess(Process Process, string[] arguments)
        {
            var aslrPolicy = new PROCESS_MITIGATION_ASLR_POLICY();
            var depPolicy = new Process_Mitigation_DEP_Policy();
            this.Process = Process;

            if (arguments.Contains("get_pid") || arguments.Contains("get_parent") || arguments.Contains("get_name") || arguments.Contains("get_integrity"))
            {
                Id = Process.Id;
            }

            if (arguments.Contains("get_description"))
            {
                Descriprion = HelpGetDescription();
            }

            if (arguments.Contains("get_path"))
            {
                PathToExe = GetMainModuleFilepath();
            }

            if (arguments.Contains("get_parent"))
            {
                PId = getppid(Id);
            }

            if (arguments.Contains("get_name"))
            {
                PName = Process.GetProcessById(Id).ProcessName;
            }

            if (arguments.Contains("get_owner"))
            {
                OwnerName = GetProcessUser(Process);
            }

            if (arguments.Contains("get_sid"))
            {
                Sid = usernameToSid(OwnerName)?.ToString();
            }

            if (arguments.Contains("get_bitness"))
            {
                try
                {
                    Bit = ProcessExtensions.IsWin64Emulator(Process) ? 64 : 32;
                }
                catch (System.ComponentModel.Win32Exception e)
                {
                    Bit = 0;
                }
            }


            if (arguments.Contains("get_dll") || arguments.Contains("get_environment"))
            {
                Dll = new ProcessDll().CollectModules(Process);
            }


            if (arguments.Contains("get_environment") && Dll != null)
            {
                Environmen = "Native";
                foreach (var w in Dll)
                {
                    if (w.ModuleName.IndexOf("mscor", 0, w.ModuleName.Length) != -1)
                    {
                        Environmen = "CLR";
                    }
                }
            }

            if (arguments.Contains("get_dep"))
            {
                try
                {
                    NativeMethods.GetProcessMitigationPolicy(Process.Handle, Process_Mitigation_Policy.ProcessDEPPolicy,
                        ref depPolicy, Marshal.SizeOf(depPolicy));

                    if (depPolicy.Flags != 0)
                    {
                        Dep = true;
                    }
                    else
                    {
                        Dep = false;
                    }
                }
                catch (System.ComponentModel.Win32Exception e)
                {
                    Dep = false;
                }
            }

            if (arguments.Contains("get_aslr"))
            {
                try
                {
                    NativeMethods.GetProcessMitigationPolicy(Process.Handle, Process_Mitigation_Policy.ProcessASLRPolicy,
                        ref aslrPolicy, Marshal.SizeOf(aslrPolicy));

                    if (aslrPolicy.Flags != 0)
                    {
                        Aslr = true;
                    }
                    else
                    {
                        Aslr = false;
                    }
                }
                catch (System.ComponentModel.Win32Exception e)
                {
                    Aslr = false;
                }
            }

            if (arguments.Contains("get_additional"))
            {
                CountHand = GetHandleCount();
                BasePrior = GetBasePriority();
                CountThread = GetCountThreads();
            }

            if (arguments.Contains("get_privileges"))
            {
                Privileges = ProcessPrivileges.ShowPrivileges(Process);
            }

            if (arguments.Contains("get_integrity"))
            {
                try
                {
                    Integrity = ConvertToString(integrityLevel(Id));
                }
                catch (System.ArgumentException)
                {
                    Integrity = "Not exist";
                }
            }
        }

        private static string GetProcessUser(Process process)
        {
            IntPtr processHandle;
            try
            {
                processHandle = IntPtr.Zero;
                OpenProcessToken(process.Handle, 8, out processHandle);
                WindowsIdentity wi = new WindowsIdentity(processHandle);
                string user = wi.Name;
                return user.Contains(@"\") ? user.Substring(user.IndexOf(@"\") + 1) : user;
            }
            catch (System.ComponentModel.Win32Exception e)
            {
                return "No permission";
            }
        }

        [DllImport("advapi32.dll", SetLastError = true)]
        private static extern bool OpenProcessToken(IntPtr ProcessHandle, uint DesiredAccess, out IntPtr TokenHandle);
        [DllImport("kernel32.dll", SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool CloseHandle(IntPtr hObject);

        public int GetHandleCount()
        {
            try
            {
                return Process.HandleCount;
            }
            catch (System.ArgumentException e)
            {
                return -1;
            }
        }

        public int GetBasePriority() => Process.BasePriority;

        public int GetCountThreads()
        {
            return Process.Threads.Count;
        }

        public string GetMainModuleFilepath()
        {
            try
            {
                return Process.MainModule.FileName;
            }
            catch (System.ComponentModel.Win32Exception e)
            {
                return "No permission";
            }
        }

    }
}
