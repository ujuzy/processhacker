﻿using System;
using System.Runtime.InteropServices;

namespace Process_Explorer
{
    public struct PROCESS_MITIGATION_ASLR_POLICY
    {
        public uint Flags;

        bool EnableBottomUpRandomization
        {
            get { return (Flags & 1) > 0; }
        }

        bool EnableForceRelocateImages
        {
            get { return (Flags & 2) > 0; }
        }

        bool EnableHighEntropy
        {
            get { return (Flags & 4) > 0; }
        }

        bool DisallowStrippedImages
        {
            get { return (Flags & 8) > 0; }
        }
    }

    public struct Process_Mitigation_DEP_Policy
    {
        public uint Flags;
        bool Permanent;

        bool Enable
        {
            get { return (Flags & 1) > 0; }
        }

        bool DisableAtlThunkEmulation
        {
            get { return (Flags & 2) > 0; }
        }
    }

    public enum Process_Mitigation_Policy
    {
        ProcessDEPPolicy = 0,
        ProcessASLRPolicy = 1
    }

    public static class NativeMethods
    {
        [DllImport("kernel32.dll", SetLastError = true, CallingConvention = CallingConvention.Winapi)]
        [return: MarshalAs(UnmanagedType.Bool)]
        internal static extern bool IsWow64Process([In] IntPtr process, [Out] out bool wow64Process);

        // Для ASLR
        [DllImport("kernel32.dll")]
        public static extern bool GetProcessMitigationPolicy(
            IntPtr hProcess,
            Process_Mitigation_Policy mitigationPolicy,
            ref Process_Mitigation_DEP_Policy lpBuffer,
            int dwLength);

        // Для DEP
        [DllImport("kernel32.dll")]
        public static extern bool GetProcessMitigationPolicy(
            IntPtr hProcess,
            Process_Mitigation_Policy mitigationPolicy,
            ref PROCESS_MITIGATION_ASLR_POLICY lpBuffer,
            int dwLength);
    }

}
