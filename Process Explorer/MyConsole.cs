﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Process_Explorer
{
    static class MyConsole
    {
        public static string[] ParseGetCommands(string arguments)
        {
            string[] separator = {" | "};

            string[] tokens = arguments.Split(separator, 15, StringSplitOptions.RemoveEmptyEntries);

            return tokens;
        }

        public static string[] ParseSetCommands(string arguments)
        {
            string[] separator = {" "};

            string[] tokens = arguments.Split(separator, 5, StringSplitOptions.RemoveEmptyEntries);

            return tokens;
        }
    }
}
