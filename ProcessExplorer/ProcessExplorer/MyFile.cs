﻿using System;
using System.Text;
using System.Collections.Generic;
using System.IO;
using System.Security.AccessControl;
using System.Security.Principal;
using Newtonsoft.Json;
using System.Runtime.InteropServices;

namespace ProcessExplorer
{
   public enum Rights
    {
        FullControl,
        AppendData,
        ReadAndExecute,
        Read,
        Write
    }

    public class MyFile
    {
        [DllImport("FileDll.dll")]
        [return: MarshalAs(UnmanagedType.I4)]
        internal static extern int integrityLevelFile(byte[] path);

        [DllImport("FileDll.dll")]
        internal static extern bool setIntegrityLevelFile(int integrityLvl, byte[] path);

        [DllImport("FileDll.dll")]
        internal static extern bool setOwner(byte[] newOwnerSid, byte[] path);

        [JsonProperty("PathFile;")]
        public string PathFile;

        [JsonProperty("Owner")]
        public string Owner { get; }

        [JsonProperty("IntegrityLevel")]
        public int IntegrityLevel { get; }

        [JsonProperty("ACL")]
        public List<string> Acl { get; }

        public byte[] PathBytes;

        public MyFile(string pathFile)
        {
            this.PathFile = pathFile;
            Owner = GetOwner();

            PathBytes = Encoding.Unicode.GetBytes(pathFile);
            IntegrityLevel = integrityLevelFile(PathBytes);

            Acl = GetAcl();
        }

        public string GetOwner()
        {
            var fs = File.GetAccessControl(PathFile);
            var sid = fs.GetOwner(typeof(SecurityIdentifier));
            var ntAccount = sid.Translate(typeof(NTAccount));
            return ntAccount.ToString();
        }

        public bool ChangeOwner(string domain, string username)
        {
            var ntAccount = new NTAccount(domain);
            var fs = File.GetAccessControl(PathFile);
            fs.SetOwner(ntAccount);
            File.SetAccessControl(PathFile, fs);
            return true;
        }

        public List<string> GetAcl()
        {
            List<string> myList = new List<string>();

            using (FileStream myFile = new FileStream(PathFile, FileMode.Open, FileAccess.Read))
            {
                FileSecurity fileSec = myFile.GetAccessControl();
                Console.WriteLine("Список ACL: ");
                foreach (FileSystemAccessRule fileRule in fileSec.GetAccessRules(true, true, typeof(NTAccount)))
                {
                    myList.Add((String.Format("{0}, {1}, {2}", fileRule.IdentityReference, fileRule.AccessControlType == AccessControlType.Allow ? "Allow" : "Deny", fileRule.FileSystemRights)));
                }
            }

            return myList;
        }

        //permission - true is Allows
        public static bool SetAcl(string account, string permission, Rights rights, string path)
        {
            var permis = AccessControlType.Allow;

            if (permission == "Deny")
            {
                permis = AccessControlType.Deny;
            }

            FileSystemAccessRule newRule = null;

            switch (rights)
            {
                case Rights.FullControl:
                    newRule = new FileSystemAccessRule(
                           new System.Security.Principal.NTAccount(account),
                           FileSystemRights.FullControl,
                           permis);
                    break;
                case Rights.AppendData:
                    newRule = new FileSystemAccessRule(
                            new System.Security.Principal.NTAccount(account),
                            FileSystemRights.AppendData,
                            permis);
                    break;
                case Rights.ReadAndExecute:
                    newRule = new FileSystemAccessRule(
                            new System.Security.Principal.NTAccount(account),
                            FileSystemRights.ReadAndExecute,
                            permis);
                    break;
                case Rights.Read:
                    newRule = new FileSystemAccessRule(
                            new System.Security.Principal.NTAccount(account),
                            FileSystemRights.Read,
                            permis);
                    break;
                case Rights.Write:
                    newRule = new FileSystemAccessRule(
                            new System.Security.Principal.NTAccount(account),
                            FileSystemRights.Write,
                            permis);
                    break;
                default:
                    return false;
            }

            using (FileStream myFile = new FileStream(
                    path, FileMode.Open, FileAccess.Read))
            {
                FileSecurity fileSec = myFile.GetAccessControl();
                fileSec.AddAccessRule(newRule);
                File.SetAccessControl(path, fileSec);
            }
            return true;
        }

        public static bool DelAcl(string domainAndName, FileSystemRights rights, string controlTypeString, string path)
        {
            AccessControlType controlType = AccessControlType.Allow;

            if (controlTypeString == "Deny")
            {
                controlType = AccessControlType.Deny;
            }

            FileSecurity fSecurity = File.GetAccessControl(path);
            fSecurity.RemoveAccessRule(new FileSystemAccessRule(domainAndName,
                rights, controlType));
            File.SetAccessControl(path, fSecurity);
            return true;
        }

        public static FileSystemRights GetAclType(string acl)
        {
            switch (acl)
            {
                case "FullControl":
                    return FileSystemRights.FullControl;
                case "AppendData":
                    return FileSystemRights.AppendData;
                case "Write":
                    return FileSystemRights.Write;
                case "ReadAndExecute":
                    return FileSystemRights.ReadAndExecute;
                case "Read":
                    return FileSystemRights.Read;
            }

            return 0;
        }

        public static SecurityIdentifier usernameToSid(string user)
        {
            try
            {
                if (user.StartsWith(@".\"))
                {
                    user = user.Replace(@".\", Environment.MachineName + @"\");
                }

                NTAccount account = new NTAccount(user);
                return (SecurityIdentifier)account.Translate(typeof(SecurityIdentifier));
            }
            catch (System.Security.Principal.IdentityNotMappedException)
            {
                return null;
            }
        }
    }
}