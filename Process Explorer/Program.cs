﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net.Mime;
using System.Runtime.InteropServices;
using System.Security.AccessControl;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Process_Explorer
{
    public class Program
    {
        [DllImport("MYDLL.dll")]
        [return: MarshalAs(UnmanagedType.I4)]
        internal static extern int setIntegrity(int s, int pid);

        [DllImport("MYDLL.dll")]
        [return: MarshalAs(UnmanagedType.I4)]
        internal static extern int integrityLevel(int pid);

        [DllImport("FileDll.dll")]
        [return: MarshalAs(UnmanagedType.I4)]
        internal static extern int integrityLevelFile(string path);

        [DllImport("FileDll.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        internal static extern bool setIntegrityLevelFile(int integrityLvl, string path);

        [DllImport("FileDll.dll")]
        internal static extern void SetLowLabelToFile(string path);

        private static List<MyProcess> mProcesses = new List<MyProcess>();

        static void Main(string[] args)
        {
            Process[] t = Process.GetProcesses();
            int q = 0;

            //var arguments = Console.ReadLine();
            string[] parsed;

            if (args[0] == "set")
            //if (false)
            {
                parsed = MyConsole.ParseSetCommands(args[1]);

                foreach (var cmd in parsed)
                { 
                    Console.WriteLine(cmd);   
                }

                if (parsed[0] == "Integrity")
                {
                    setIntegrity(MyProcess.ConvertToCode(parsed[2]), Int32.Parse(parsed[1]));
                }

                if (parsed[0] == "Privilege")
                {
                    if (parsed[3] == "Enable")
                    {
                        ProcessPrivileges.EnablePriv(Process.GetProcessById(Int32.Parse(parsed[1])), parsed[2]);
                    }

                    if (parsed[3] == "Disable")
                    {
                        ProcessPrivileges.DisablePriv(Process.GetProcessById(Int32.Parse(parsed[1])), parsed[2]);
                    }
                }

                if (parsed[0] == "acl")
                {
                    //MyFile.SetAcl(parsed[2], parsed[4], )

                    if (parsed[3] == "append_data")
                    {
                        MyFile.SetAcl(parsed[2], parsed[4], Rights.AppendData, parsed[1]);
                    }

                    if (parsed[3] == "full_control")
                    {
                        MyFile.SetAcl(parsed[2], parsed[4], Rights.FullControl, parsed[1]);
                    }

                    if (parsed[3] == "read")
                    {
                        MyFile.SetAcl(parsed[2], parsed[4], Rights.Read, parsed[1]);
                        Console.WriteLine("Read denied");
                    }

                    if (parsed[3] == "read_and_execute")
                    {
                        MyFile.SetAcl(parsed[2], parsed[4], Rights.ReadAndExecute, parsed[1]);
                    }

                    if (parsed[3] == "write")
                    {
                        MyFile.SetAcl(parsed[2], parsed[4], Rights.Write, parsed[1]);
                    }
                }

                if (parsed[0] == "del_acl")
                {
                    if (parsed[3] == "append_data")
                    {
                        MyFile.DelAcl(parsed[2], FileSystemRights.AppendData, parsed[4], parsed[1]);
                    }

                    if (parsed[3] == "full_control")
                    {
                        MyFile.DelAcl(parsed[2], FileSystemRights.FullControl, parsed[4], parsed[1]);
                    }

                    if (parsed[3] == "read")
                    {
                        MyFile.DelAcl(parsed[2], FileSystemRights.Read, parsed[4], parsed[1]);
                    }

                    if (parsed[3] == "read_and_execute")
                    {
                        MyFile.DelAcl(parsed[2], FileSystemRights.ReadAndExecute, parsed[4], parsed[1]);
                    }

                    if (parsed[3] == "write")
                    {
                        MyFile.DelAcl(parsed[2], FileSystemRights.Write, parsed[4], parsed[1]);
                    }
                }

                Console.WriteLine("Set end");
            }
            else
            {
                parsed = MyConsole.ParseGetCommands(args[0]);

                //var parsedSet = MyConsole.ParseSetCommands(args[0]);

                foreach (Process i in t)
                {
                    try
                    {
                        MyProcess check = new MyProcess(i, parsed);
                        mProcesses.Add(check);
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e.Message);
                        q++;
                    }
                }

                using (StreamWriter sw = new StreamWriter("process.json", false, System.Text.Encoding.Default))
                {
                    var toFile = JsonConvert.SerializeObject(mProcesses, Formatting.Indented);
                    sw.Write(toFile);
                }

                Console.WriteLine("else set end");
            }

            Console.WriteLine("Program end");

            Console.WriteLine(q);
            int a;
            Console.ReadLine();
        }
    }
}
