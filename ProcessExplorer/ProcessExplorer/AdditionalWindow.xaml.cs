﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;

namespace ProcessExplorer
{
    public partial class Additional : Window
    {
        private MyProcess mProcess;

        public Additional(MyProcess process)
        {
            InitializeComponent();

            mProcess = process;

            processName.Text = mProcess.Name;
            processHandles.Text = mProcess.CountHand.ToString();
            processPriority.Text = mProcess.BasePrior.ToString();
            processThreads.Text = mProcess.CountThread.ToString();

            additionalList.ItemsSource = mProcess.DLL;
            privilegesList.ItemsSource = mProcess.Privileges;
        }

        private void MenuItemAdditional_OnClick(object sender, RoutedEventArgs e)
        {
            var arguments = privilegesList.SelectedItem.ToString();
            string[] separator = {"[", "]", ",", " "};
            string[] tokens = arguments.Split(separator, 3, StringSplitOptions.RemoveEmptyEntries);

            string value = "";

            if (tokens[1] == "Enabled")
            {
                value = "Disable";

                mProcess.Privileges[tokens[1]] = "Disabled";
            }

            if (tokens[1] == "Disabled")
            {
                value = "Enable";

                mProcess.Privileges[tokens[1]] = "Enabled";
            }

            string consoleArguments = $"set \"Privilege {mProcess.PID} {tokens[0]} {value}\"";

            Controller.StartConsole(Globals.ConsoleAppPath, consoleArguments);
        }
    }
}