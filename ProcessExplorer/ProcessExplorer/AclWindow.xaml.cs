﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ProcessExplorer
{
    /// <summary>
    /// Interaction logic for AclWindow.xaml
    /// </summary>
    public partial class AclWindow : Window
    {
        private MyFile mFile;

        public AclWindow(MyFile file)
        {
            InitializeComponent();

            mFile = file;
        }

        private void ButtonBase_OnClick(object sender, RoutedEventArgs e)
        {
            var user = userName.Text;
            var aclType = Controller.StringToRights(aclBox.Text);
            var permission = accessBox.Text;

            MyFile.SetAcl(user, permission, aclType, mFile.PathFile);
        }

        private void MenuItemFullControl_OnClick(object sender, RoutedEventArgs e)
        {
            aclBox.Text = "Full Control";
        }

        private void MenuItemAppendData_OnClick(object sender, RoutedEventArgs e)
        {
            aclBox.Text = "Append Data";
        }

        private void MenuItemReadAndExecute_OnClick(object sender, RoutedEventArgs e)
        {
            aclBox.Text = "Read And Execute";
        }

        private void MenuItemRead_OnClick(object sender, RoutedEventArgs e)
        {
            aclBox.Text = "Read";
        }

        private void MenuItemWrite_OnClick(object sender, RoutedEventArgs e)
        {
            aclBox.Text = "Write";
        }

        private void MenuItemAllow_OnClick(object sender, RoutedEventArgs e)
        {
            accessBox.Text = "Allow";
        }

        private void MenuItemDeny_OnClick(object sender, RoutedEventArgs e)
        {
            accessBox.Text = "Deny";
        }
    }
}
