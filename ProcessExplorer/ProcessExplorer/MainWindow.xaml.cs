﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Windows;

namespace ProcessExplorer
{
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            GetListViewItems();
        }

        private void GetListViewItems()
        {
            List<MyProcess> processes = Controller.GetFromJson();

            processesList.ItemsSource = processes;
        }

        private void MenuItemAdditional_OnClick(object sender, RoutedEventArgs e)
        {
            var process = processesList.SelectedItem as MyProcess;

            Additional newWindow = new Additional(process);

            newWindow.Show();
        }

        private void ButtonGetFilePath_OnClick(object sender, RoutedEventArgs e)
        {
            string path = filePathBox.Text;

            try
            {
                File.Exists(path);

                FileInfo newWindow = new FileInfo(path);

                newWindow.Show();
            }
            catch
            {
                filePathBox.Text = "Error - File not exist";
            }
        }

        private void ButtonRefresh_OnClick(object sender, RoutedEventArgs e)
        {
            GetListViewItems();
        }

        private void MenuItemIntegrityUntrusted_OnClick(object sender, RoutedEventArgs e)
        {
            var process = processesList.SelectedItem as MyProcess;

            Controller.StartConsole(Globals.ConsoleAppPath, $"set \"Integrity {process.PID} Untrusted\"");
        }

        private void MenuItemIntegrityLow_OnClick(object sender, RoutedEventArgs e)
        {
            var process = processesList.SelectedItem as MyProcess;

            Controller.StartConsole(Globals.ConsoleAppPath, $"set \"Integrity {process.PID} Low\"");
        }

        private void MenuItemIntegrityMedium_OnClick(object sender, RoutedEventArgs e)
        {
            var process = processesList.SelectedItem as MyProcess;

            Controller.StartConsole(Globals.ConsoleAppPath, $"set \"Integrity {process.PID} Medium\"");
        }

        private void MenuItemIntegrityHigh_OnClick(object sender, RoutedEventArgs e)
        {
            var process = processesList.SelectedItem as MyProcess;

            Controller.StartConsole(Globals.ConsoleAppPath, $"set \"Integrity {process.PID} High\"");
        }
    }
}