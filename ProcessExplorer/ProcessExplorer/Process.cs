﻿using System;
using System.Security.Principal;
using System.Diagnostics;
using Newtonsoft.Json;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;

namespace ProcessExplorer
{
    public class MyProcess
    {
        [JsonProperty("Id")]
        public int PID { get; set; }

        [JsonProperty("Descriprion")]
        public string Descriprion { get; set; }

        [JsonProperty("PathToExe")]
        public string Path { get; set; }

        [JsonProperty("PName")]
        public string Name { get; set; }

        [JsonProperty("PId")]
        public int Parent { get; set; }

        [JsonProperty("OwnerName")]
        public string OwnerName { get; set; }

        [JsonProperty("Sid")]
        public string SID { get; set; }

        [JsonProperty("Bit")]
        public int Bitness { get; set; }

        [JsonProperty("Environmen")]
        public string Environmen { get; set; }

        [JsonProperty("Dep")]
        public bool Dep { get; set; } 

        [JsonProperty("Aslr")]
        public bool Aslr { get; set; }

        [JsonProperty("Dll")]
        public List<Module> DLL { get; set; }

        [JsonProperty("CountHand")]
        public int CountHand { get; set; }

        [JsonProperty("CountThread")]
        public int CountThread { get; set; }

        [JsonProperty("BasePrior")]
        public int BasePrior { get; set; }

        [JsonProperty("Privileges")]
        public Dictionary<string, string> Privileges { get; set; }

        [JsonProperty("Integrity")]
        public string Integrity { get; set; }
    }
}